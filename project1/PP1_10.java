/*
Write a program that displays the word �HI� in large block
letters. Make each large letter out of the other character.
(H out of I's; I out of H's)
*/

package project1;

public class PP1_10 {

	public static void main(String[] args)
	{
		System.out.println("I I I       I I I    H H H H H H H H H");
		System.out.println("I I I       I I I            H        ");
		System.out.println("I I I       I I I            H        ");
		System.out.println("I I I       I I I            H        ");
		System.out.println("I I I I I I I I I            H        ");
		System.out.println("I I I       I I I            H        ");
		System.out.println("I I I       I I I            H        ");
		System.out.println("I I I       I I I            H        ");
		System.out.println("I I I       I I I    H H H H H H H H H");
	}
	
}
