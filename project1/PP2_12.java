/*
Write a program that prompts for and reads two integers representing
the length and breadth of a rectangle, then prints its
perimeter and area.
*/

package project1;

import java.util.Scanner;

public class PP2_12 {

	public static void main(String[] args)
	{
		int length, width, area, perimeter;
				
		Scanner scan = new Scanner(System.in);
		
		System.out.print("Enter the length of the rectangle: ");
		length = scan.nextInt();
		
		System.out.print("Enter the width of the rectangle: ");
		width = scan.nextInt();
		
		scan.close();
		
		perimeter = (2 * (length + width));
		area = length * width;
		
		System.out.println("The rectangle's perimeter is: " +  perimeter);
		System.out.println("The rectangle's area is: " + area);
	}
}
