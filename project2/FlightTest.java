package project2;

public class FlightTest {

    public static void main(String[] args)
    {
        Flight flight1 = new Flight("Southwest", "Orlando", "Las Vegas", 8031093);
        Flight flight2 = new Flight("Delta", "Ontario", "Toronto", 9045012);
        Flight flight3 = new Flight("Delta", "New York", "Los Angeles", 8012345);

        flight3.setFltNum(4024350);
        flight2.setName("American");
        
        System.out.println(flight1.toString());
        System.out.println("\n" + flight2.toString());
        System.out.println("\n" + flight3.toString());

        System.out.println("\n" + flight1.getName() + " flight " + flight1.getFltNum() + " has departed " + flight1.getOrgCity() + " and is on its way to " + flight1.getDestCity());
        System.out.println("Well, we're gonna hijack it.");

        flight1.setDestCity("Mordor");

        System.out.println(flight1.getName() + " flight " + flight1.getFltNum() + " is now on its way to " + flight1.getDestCity());
    }

}