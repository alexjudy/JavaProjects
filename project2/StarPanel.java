package project2;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.util.Random;

import javax.swing.JPanel;

@SuppressWarnings("serial")

public class StarPanel extends JPanel{
	
	private Star star1, star2, star3, star4, star5, star6;
	
	public StarPanel(){
		
/*
// Uncomment and change objects manually here					
		star1 = new Star(40, 50, 100);
		star2 = new Star(300, 250, 20);
		star3 = new Star(60, 70, 90);
		star4 = new Star(100, 50, 80);
		star5 = new Star(450, 200, 50);
		star6 = new Star(350, 10, 150);
*/
		
		Random rnd = new Random();
		int x, y, width;
		
		//Create random integers every time an object is set. Could have
		// used a for loop for this. 
		x = rnd.nextInt(500) + 1;
		y = rnd.nextInt(400) + 1;
		width = rnd.nextInt(500) + 1;
		star1 = new Star(x, y, width);
		x = rnd.nextInt(500) + 1;
		y = rnd.nextInt(400) + 1;
		width = rnd.nextInt(500) + 1;
		star2 = new Star(x, y, width);
		x = rnd.nextInt(500) + 1;
		y = rnd.nextInt(400) + 1;
		width = rnd.nextInt(500) + 1;
		star3 = new Star(x, y, width);
		x = rnd.nextInt(500) + 1;
		y = rnd.nextInt(400) + 1;
		width = rnd.nextInt(500) + 1;
		star4 = new Star(x, y, width);
		x = rnd.nextInt(500) + 1;
		y = rnd.nextInt(400) + 1;
		width = rnd.nextInt(500) + 1;
		star5 = new Star(x, y, width);
		x = rnd.nextInt(500) + 1;
		y = rnd.nextInt(400) + 1;
		width = rnd.nextInt(500) + 1;
		star6 = new Star(x, y, width);
		
		setPreferredSize(new Dimension(500, 500));
		setBackground(Color.black);
	}
	
	public void paintComponent(Graphics page)
	{
		super.paintComponent(page);
		
		star1.drawStar(page);
		star2.drawStar(page);
		star3.drawStar(page);
		star4.drawStar(page);
		star5.drawStar(page);
		star6.drawStar(page);	
	}
}
