package project3;

import java.awt.*;
import java.util.*;
import javax.swing.*;

@SuppressWarnings("serial")

public class HorizontalLines extends JPanel {

	private int x1 = 10, x2 = 0, y = 10;
	private int length = 0;
	Random rand = new Random();
	
	public HorizontalLines(){
		setBackground(Color.yellow);
		setPreferredSize(new Dimension(500, 500));
	}
	
	public void paintComponent(Graphics page){
		super.paintComponent(page);
		
		for (int i = 0; i < 20; i++){
			length = rand.nextInt(480);
			
			x2 = x1 + length; //Create a random length each 
			
			page.drawLine(x1, y, x2, y);
			
			y += 10; //Add to y to create spacing after each loop
		}
	}
}
