package project3;

import javax.swing.JFrame;

public class HorizontalLinesApp {
	
	public static void main(String [] args){
		JFrame frame = new JFrame("Parallel Lines");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		frame.getContentPane().add(new HorizontalLines());

		frame.pack();
		frame.setVisible(true);
	}
}
